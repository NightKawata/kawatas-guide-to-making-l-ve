Kawata's Guide to Making LÖVE -  LESSON 1: IT STARTS
==
Alright well I already fucked up with the first commit message. THIS is the first lesson. Now that you have the utensils you need to make some LÖVE, let's begin.

We're going to want to learn how to use Lua, however. Since, you know, LÖVE requires you master the sun and moon before you touch its heavenly body. (I get paid to write nonsense)

    WARNING: KAWATA IS STUPID AND THERE ARE BETTER WAYS TO LEARN LUA
    http://www.lua.org/pil/
    The above link will teach you more about Lua than I ever will. However,
    I will do my best to teach you as I go along.
    
Now that you've been thoroughly warned, I shall begin to teach you.
If you know how to program already, Lua is a familiar face, and all you'll need to learn about are tables.

Now if you haven't, I'll give you a 5 minute crash course on it. (this isn't good)
Variable usage: Every variable declared is global unless you declare it as local. ...Yeah that sucks just look at the example

            "my_global = 5"
            "local my_value = 5"

You can't pull +=, -=, /=, or anything like that with Lua, just do:

    x = x + 1
    x = x - 1
    x = x * 1
    x = x / 1
    
Lua's also one-indexed: Make sure you know this! Plus, tables. _Oh man, tables..._

I could get into a huge discussion about the beauty of Lua tables and how it's one of Lua's best qualities, but nobody wants to be here for an hour. Long story short, some look like this: 

    local my_table = {}
    local my_array = {
        3,
        4,
        5,
        "a lot",
        }

Yeah I'm bad with explaining how to use those. I'll cross that bridge when I come to it.
[Just go here. It explains it far better than I can.](http://www.lua.org/pil/2.5.html)

Anyways, Lesson 2 will be much more organized than this mess, and if I haven't killed you off yet, we'll explain stuff and get into basic handling of LÖVE next. I also teach you how to have sex. If that isn't reason enough, I don't know what is.