Kawata's Guide to Making LÖVE
==
Well. It's come down to this. I suppose you'd like to learn how to make some LÖVE. 

Well then, grab a seat, maybe a condom too, and possibly your favorite copy of Mark Twain. This will be an... interesting journey. You'll want the following tools for this guide:

* A text editor. I'd recommend Notepad++ for Windows, and Sublime Text 2 will also work. (It's even cross platform!)
* LÖVE: http://love2d.org/ (I won't cover how to get that onto your computer, thanks Linux)
* Free time. I mean, how do you expect to make oh so sweet LÖVE without it?

Now that you've read this apocalyptic mess, you're ready to start the first lesson!